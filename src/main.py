
from kivy.app import App

# from kivy.uix.scatter import Scatter
# from kivy.uix.label import Label
# from kivy.uix.floatlayout import FloatLayout
# from kivy.uix.textinput import TextInput
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image
from kivy.properties import NumericProperty
# from kivy.uix.button import Button
from kivy_garden.graph import Graph, LinePlot

import numpy as np



class Plotter(BoxLayout):

    zoom = NumericProperty()
    
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.samples = 512
        self.zoom = 1
        self.graph = Graph(xmin=0, xmax=self.samples,
                            ymin=-1, ymax=1,
                            border_color=[0,1,1,1],
                            tick_color=[0,1,1,0.7],
                            x_grid=True, y_grid=True,
                            draw_border=False,
                            x_grid_label=True, y_grid_label=False,
                            x_ticks_major=64,y_ticks_major=0.5, )
        self.ids.graph.add_widget(self.graph)
        self.plot_x = np.linspace(0,1, self.samples)
        self.plot_y = np.sin(2*np.pi*self.plot_x)
        self.plot = LinePlot(color=[1,1,0,1], line_width=1)
        self.plot.points = [(x,self.plot_y[x]) for x in range(self.samples)]
        self.graph.add_plot(self.plot)

    def update_plot(self, freq):
        self.plot_y = np.sin(2*np.pi*freq*self.plot_x)
        self.plot.points = [(x,self.plot_y[x]) for x in range(self.samples)]


    def update_zoom(self, value):
        if value == '+' and self.zoom <8:
            self.zoom*= 2
            self.graph.x_ticks_major/=2

        if value == '-' and self.zoom >1:
            self.zoom/= 2
            self.graph.x_ticks_major*=2

class TrackerApp(App):
    """
    Main Application class
    """
    def build(self):

        return Plotter()

if __name__ == "__main__":
    TrackerApp().run()