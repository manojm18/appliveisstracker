# Live ISS Tracker App using [kivy](https://kivy.org/)

## Environment setup on Windows

### Virtual Env
In an empty project directory, create a python virtualenv and activate it:
- `python -m venv venv`
- `.\venv\Scripts\activate.bat`

### Dependencies
Install Kivy + deps
- `python -m pip install kivy cython`
- `pip install kivy_garden.graph --extra-index-url https://kivy-garden.github.io/simple/`

### Run in developer mode

- `python src/main.py` will run your app in dev mode

### For Android: to generate apk file
From `<YOUR_PROJECT_DIR>`, run
- `buildozer android debug`
